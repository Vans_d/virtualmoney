﻿using System.Data.Entity;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Async;
using Autofac;
using Autofac.Integration.Mvc;
using Microsoft.Owin.Security.OAuth;
using ParrotWings.App;
using Data;

[assembly: PreApplicationStartMethod(typeof(AppStart), "Run")]

namespace ParrotWings.App
{
    public class AppStart
    {
        public static void Run()
        {
            var builder = new ContainerBuilder();

            ConfigureBuilder(builder);


            builder.RegisterSource(new ViewRegistrationSource());

            var container = builder.Build();
            SetDependencyResolver(container);

            using (var context = new DataContext())
            {
                context.Database.Initialize(false);
            }
        }

        public static void ConfigureBuilder(ContainerBuilder builder)
        {
            RegisterTypes(builder);

            builder.RegisterModule(new AutofacWebTypesModule());
        }

        private static void RegisterTypes(ContainerBuilder builder)
        {
            // web api
            builder.RegisterModule<AutofacWebTypesModule>();
            builder.RegisterControllers(typeof(AppStart).Assembly).PropertiesAutowired();
        }

        private static void SetDependencyResolver(IContainer container)
        {
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}