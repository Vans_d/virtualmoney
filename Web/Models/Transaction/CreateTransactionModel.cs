﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Web.Models.Transaction
{
    public class CreateTransactionModel
    {
        public int SelectedrecipientId { get; set; }

        public IEnumerable<Data.Entities.User> recipients { get; set; }

        [Required]
        [Range(0, 10000000, ErrorMessage = "Сумма не может быть отрицательной")]
        public decimal Sum { get; set; }
    }
}