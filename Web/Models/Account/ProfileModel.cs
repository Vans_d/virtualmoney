﻿using System;

namespace Web.Models.Account
{
    public class ProfileModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime CreatedAt { get; set; }
        public decimal Balance { get; set; }

        public string FullName => $"{FirstName} {LastName}";
    }
}