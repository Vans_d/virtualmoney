﻿using System.ComponentModel.DataAnnotations;

namespace Web.Models.Account
{
    public class RegisterModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "Значение не должно превышать более {0} символов")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Значение не должно превышать более {0} символов")]
        public string LastName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Значение не должно превышать более {0} символов")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Значение должно содержать не менее {2} символов.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Пароль и его подтверждение не совпадают.")]
        public string ConfirmPassword { get; set; }
    }
}