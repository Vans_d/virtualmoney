﻿namespace Web.Models.WebApi
{
    public class Response<TError, TResult>
    {
        public TError Error { get; set; }
        public TResult Result { get; set; }
    }
}