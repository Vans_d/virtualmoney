﻿using System;

namespace Web.Models.WebApi
{
    public class TransactionModel
    {
        public DateTime CreatedAt { get; set; }
        public string SenderFullName { get; set; }
        public string RecipientFullName { get; set; }
        public decimal Sum { get; set; }
        public decimal SenderBalanceAfter { get; set; }
        public decimal RecipientBalanceAfter { get; set; }
        public int UserId { get; set; }
    }
}