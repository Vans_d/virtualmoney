﻿namespace Web.App.Enums
{
    public enum LoginError
    {
        None = 0,
        UnknownError = 1,
        InvalidEmailOrPassword = 2,
        EmptyField = 3
    }
}