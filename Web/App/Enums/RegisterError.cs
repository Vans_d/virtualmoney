﻿namespace Web.App.Enums
{
    public enum RegisterError
    {
        None = 0,
        UnknownError = 1,
        DuplicateEmail = 2,
        ShortPassword = 3,
        NoMatchPasswords = 4,
        EmptyField = 5,
        InvalidEmail = 6
    }
}