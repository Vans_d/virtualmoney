﻿using System;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace Web.App.Extensions
{
    public static class HtmlExtensions
    {
        public static MvcHtmlString TextBoxFieldFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            var html = htmlHelper.TextBoxFor(expression, new { @class = "form-control" });
            var result = new Regex(@"value=""[\d\s\w/:.]+""").Replace(html.ToString(), "value=\"\"");
            
            return MvcHtmlString.Create(result);
        }

        public static MvcHtmlString PasswordFieldFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            return htmlHelper.PasswordFor(expression, new { @class = "form-control" });
        }
    }
}
