﻿using System.Linq;
using System.Web.Mvc;
using Services.Interfaces;
using Web.App;

namespace Web.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        public ITransactionService TransactionService { get; set; }

        public ActionResult Index()
        {
            var transactions = TransactionService.GetTransactionsByUserId(CurrentUser.Get().Id).ToArray();
            return View(transactions);
        }
    }
}
