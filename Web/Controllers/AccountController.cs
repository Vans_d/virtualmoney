﻿using System.Web.Mvc;
using Services.Interfaces;
using Web.App;
using Web.Models.Account;

namespace Web.Controllers
{
    public class AccountController: Controller
    {
        public IUserService UserService { get; set; }

        public ActionResult Login()
        {
            var model = new LoginModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                var user = UserService.GetUserByEmail(model.Email);

                if (UserService.ValidateUser(model.Email, model.Password, out user))
                {
                    IdentityHelper.IdentitySignin(new AppUserState(user));
                    return RedirectToAction("Index", "Home");
                }

                ModelState.AddModelError("Password", "Неверный логин и/или пароль");
                return View(model);
            }
            return View(model);
        }

        public ActionResult Register()
        {
            return View(new RegisterModel());
        }

        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                if (UserService.IsExistEmail(model.Email))
                {
                    ModelState.AddModelError("Email", "Пользователь с таким email уже существует");
                    return View(model);
                }

                var user = UserService.CreateUser(model.FirstName, model.LastName, model.Email, model.Password);

                IdentityHelper.IdentitySignin(new AppUserState(user));

                return RedirectToAction("Index", "Home");
            }
            return View(model);
        }

        public ActionResult Logout()
        {
            IdentityHelper.IdentitySignout();
            return RedirectToAction("Index", "Home");
        }
    }
}