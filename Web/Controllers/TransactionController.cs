﻿using System.Web.Mvc;
using Services.Interfaces;
using Web.App;
using Web.Models.Transaction;

namespace Web.Controllers
{
    public class TransactionController : Controller
    {
        public IUserService UserService { get; set; }
        public ITransactionService TransactionService { get; set; }

        public ActionResult CreateTransaction()
        {
            var recipients = UserService.GetAllRecipients(CurrentUser.Get().Id);

            var model = new CreateTransactionModel { recipients = recipients };

            return View("Create", model);
        }

        [HttpPost]
        public ActionResult CreateTransaction(CreateTransactionModel m)
        {
            var user = CurrentUser.Get();
            m.recipients = UserService.GetAllRecipients(user.Id);

            if (!ModelState.IsValid)
                return View("Create", m);

            if (m.Sum >= user.Balance)
            {
                ModelState.AddModelError("Sum", "Сумма к отправке превышает Ваш баланс");
                return View("Create", m);
            }

            TransactionService.CreateTransaction(user.Id, m.SelectedrecipientId, m.Sum);

            return RedirectToAction("Index", "Home");
        }

        public ActionResult CopyTransaction(int transactId)
        {
            var userId = CurrentUser.Get().Id;
            var recipients = UserService.GetAllRecipients(userId);
            var transaction = TransactionService.GetTransaction(transactId);

            var model = new CreateTransactionModel()
            {
                recipients = recipients,
                SelectedrecipientId = transaction.RecipientId,
                Sum = transaction.Sum
            };

            return View("Create", model);
        }

        [HttpPost]
        public ActionResult CopyTransaction(CreateTransactionModel m)
        {
            var user = CurrentUser.Get();
            m.recipients = UserService.GetAllRecipients(user.Id);

            if (!ModelState.IsValid)
                return View("Create", m);

            if (m.Sum >= user.Balance)
            {
                ModelState.AddModelError("Sum", "Сумма к отправке превышает Ваш баланс");
                return View("Create", m);
            }

            TransactionService.CreateTransaction(user.Id, m.SelectedrecipientId, m.Sum);

            return RedirectToAction("Index", "Home");
        }
    }
}