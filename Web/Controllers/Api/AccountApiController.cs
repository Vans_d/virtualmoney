﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Http;
using Services.Interfaces;
using Web.App.Enums;
using Web.Models.Account;
using Web.Models.WebApi;

namespace Web.Controllers.Api
{
    [RoutePrefix("api/Account")]
    public class AccountApiController : ApiController
    {
        public IUserService UserService { get; set; }
        public IWalletService WalletService { get; set; }

        [HttpGet]
        [Route("Register")]
        public Response<RegisterError, string> Register([FromUri]RegisterModel model)
        {
            var response = new Response<RegisterError, string>();

            try
            {
                if (string.IsNullOrEmpty(model.FirstName) || string.IsNullOrEmpty(model.Email) ||
                    string.IsNullOrEmpty(model.Password) || string.IsNullOrEmpty(model.ConfirmPassword))
                {
                    response.Error = RegisterError.EmptyField;
                    return response;
                }
                if (!IsValidEmail(model.Email))
                {
                    response.Error = RegisterError.InvalidEmail;
                    return response;
                }

                if (!model.Password.Equals(model.ConfirmPassword))
                {
                    response.Error = RegisterError.NoMatchPasswords;
                    return response;
                }

                if (model.Password.Length < 6)
                {
                    response.Error = RegisterError.ShortPassword;
                    return response;
                }

                if (UserService.IsExistEmail(model.Email))
                {
                    response.Error = RegisterError.DuplicateEmail;
                    return response;
                }

                var user = UserService.CreateUser(model.FirstName, model.LastName, model.Email, model.Password);

                response.Error = RegisterError.None;
                response.Result = user.Token.ToString();
            }
            catch
            {
                response.Error = RegisterError.UnknownError;
            }
            return response;
        }

        [HttpGet]
        [Route("Login")]
        public Response<LoginError, string> Login([FromUri]LoginModel model)
        {
            var response = new Response<LoginError, string>();

            try
            {
                if (string.IsNullOrEmpty(model.Email) || string.IsNullOrEmpty(model.Password))
                {
                    response.Error = LoginError.EmptyField;
                    return response;
                }
                if (!UserService.ValidateUser(model.Email, model.Password))
                {
                    response.Error = LoginError.InvalidEmailOrPassword;
                    return response;
                }

                var user = UserService.GetUserByEmail(model.Email);

                response.Error = LoginError.None;
                response.Result = user.Token.ToString();
            }
            catch (Exception ex)
            {
                response.Error = LoginError.UnknownError;
            }
            return response;
        }

        [HttpGet]
        [Route("GetUser")]
        public Response<int, ProfileModel> GetUser(string token)
        {
            var response = new Response<int, ProfileModel>();

            try
            {
                if (ValidateToken(token))
                {
                    Guid tokenGuid;
                    Guid.TryParse(token, out tokenGuid);
                    var user = UserService.GetUserByToken(tokenGuid);
                    var userModel = new ProfileModel()
                    {
                        Id = user.Id,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        Email = user.Email,
                        Balance = WalletService.GetWalletByUserId(user.Id).Balance
                    };
                    response.Error = 0;
                    response.Result = userModel;
                }

                return response;

            }
            catch (Exception ex)
            {
                response.Error = 0;
            }
            return response;
        }

        [HttpGet]
        [Route("GetAllRecipients")]
        public Response<int, IList<ProfileModel>> GetAllRecipients(int userId)
        {
            var response = new Response<int, IList<ProfileModel>>();

            try
            {
                var users = UserService.GetAllRecipients(userId).ToArray();
                var recipients = users.Select(user => new ProfileModel
                {
                    Id = user.Id,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Email = user.Email,
                    Balance = WalletService.GetWalletByUserId(user.Id).Balance
                }).ToList();

                response.Error = 0;
                response.Result = recipients;

                return response;
            }
            catch { response.Error = 1; }

            return response;
        }

        private bool ValidateToken(string token)
        {
            Guid tokenGuid;
            if (!Guid.TryParse(token, out tokenGuid) || UserService.GetUserByToken(tokenGuid) == null)
                return false;

            return true;
        }

        private bool IsValidEmail(string email)
        {
            string pattern = @"^[-a-zA-Z0-9][-.a-zA-Z0-9]*@[-.a-zA-Z0-9]+(\.[-.a-zA-Z0-9]+)*\.
                (com|edu|info|gov|int|mil|net|org|biz|name|museum|coop|aero|pro|tv|ru|рф|[a-zA-Z]{2})$";
            Regex check = new Regex(pattern, RegexOptions.IgnorePatternWhitespace);

            var valid = !string.IsNullOrEmpty(email) && check.IsMatch(email);

            return valid;
        }
    }
}