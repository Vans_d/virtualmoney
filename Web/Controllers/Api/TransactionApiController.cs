﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Web.Models.WebApi;
using Services.Interfaces;

namespace Web.Controllers.Api
{
    [RoutePrefix("api/Transaction")]
    public class TransactionApiController : ApiController
    {
        public IUserService UserService { get; set; }
        public IWalletService WalletService { get; set; }
        public ITransactionService TransactionService { get; set; }

        [HttpGet]
        [Route("GetTransactions")]
        public Response<int, IList<TransactionModel>> GetTransactions(int userId)
        {
            var response = new Response<int, IList<TransactionModel>>();

            try
            {
                var transactions = TransactionService.GetTransactionsByUserId(userId).ToArray();
                var transacts = new List<TransactionModel>();
                if (transactions.Any())
                {
                    transacts.AddRange(transactions.Select(transaction => new TransactionModel
                    {
                        CreatedAt = transaction.CreatedAt,
                        Sum = transaction.Sum,
                        SenderFullName = UserService.GetUserById(transaction.SenderId).FullName,
                        RecipientFullName = UserService.GetUserById(transaction.RecipientId).FullName,
                        SenderBalanceAfter = transaction.SenderBalanceAfter,
                        RecipientBalanceAfter = transaction.RecipientBalanceAfter,
                        UserId = transaction.SenderId
                    }));
                    response.Error = 0;
                    response.Result = transacts;
                }
                else
                    response.Error = 1;

                return response;

            }
            catch (Exception ex)
            {
                response.Error = 0;
            }
            return response;
        }

        [HttpGet]
        [Route("CreateTransaction")]
        public bool CreateTransaction([FromUri] TransactModel m)
        {
            var balanceUser = WalletService.GetWalletByUserId(m.SenderId).Balance;
            var sumOrder = Convert.ToDecimal(m.Sum);
            if (balanceUser < sumOrder)
                return false;
            TransactionService.CreateTransaction(m.SenderId, m.RecipientId, sumOrder);
            return true;
        }

    }

    public class TransactModel
    {
        public int SenderId { get; set; }
        public int RecipientId { get; set; }
        public string Sum { get; set; }
    }
}