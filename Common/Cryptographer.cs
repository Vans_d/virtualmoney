﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Common
{
    public static class Cryptographer
    {
        public static string ComputeSha512Hash(string valueToHash)
        {
            var algorithm = SHA512.Create();
            var hash = algorithm.ComputeHash(Encoding.UTF8.GetBytes(valueToHash));
            return Convert.ToBase64String(hash);
        }
    }
}
