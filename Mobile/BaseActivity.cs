using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Mobile.Contexts;
using Mobile.Page;

namespace Mobile
{
    [Activity(Label = "BaseActivity")]
    public abstract class BaseActivity : AppCompatActivity
    {
        private FirstPage _firstPage;
        private IndexPage _indexPage;

        protected abstract int LayoutResource { get; }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(LayoutResource);
            if (UserContext.IsLoggedIn())
            {
                _indexPage = new IndexPage();
                SupportFragmentManager.BeginTransaction().Add(Resource.Id.main, _indexPage).Commit();
            }
            else
            {
                _firstPage = new FirstPage();
                SupportFragmentManager.BeginTransaction().Add(Resource.Id.mainFirst, _firstPage).Commit();
            }
        }
    }
}