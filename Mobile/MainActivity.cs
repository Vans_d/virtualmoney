﻿using Android.App;
using Android.Widget;
using Android.OS;
using Mobile.Contexts;

namespace Mobile
{
    [Activity(Label = "Mobile", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : BaseActivity
    {
        protected override int LayoutResource => UserContext.IsLoggedIn() ? Resource.Layout.Main : Resource.Layout.FirstPage;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
        }
    }
}

