using System.Linq;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using Fragment = Android.Support.V4.App.Fragment;
using Mobile.Contexts;
using Mobile.Helper;
using Mobile.Model;

namespace Mobile.Page
{
    public class CreateTransaction : BaseFragment
    {
        private RecipientsModel _recipients;
        private int _selectedCorrId;
        private User _user;
        public static Fragment newInstance(Context context)
        {
            CreateTransaction createTransaction = new CreateTransaction();
            return createTransaction;
        }
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            ViewGroup root = (ViewGroup)inflater.Inflate(Resource.Layout.CreateTransaction, null);

            _user = Service.Users.GetUser(UserContext.GetToken()).Result;

            var userName = root.FindViewById<TextView>(Resource.Id.userName);
            userName.TextSize = 16;
            userName.Text = "������������, " + _user.FullName + "!";

            var ballance = root.FindViewById<TextView>(Resource.Id.balance);
            ballance.Text = "��� ������:" + _user.Balance;

            var exit = root.FindViewById<TextView>(Resource.Id.exit);
            exit.Text = "�����";

            _recipients = Service.Users.GetAllRecipients(_user.Id);

            if (_recipients.Result != null)
            {
                var spinner = root.FindViewById<Spinner>(Resource.Id.spinner_cor);
                spinner.Adapter = new UsersAdapter(Activity, _recipients.Result.ToArray());
                spinner.ItemSelected  += OnSpinnerItemSelect;
            }
            
            var btnCreateTransact = root.FindViewById<Button>(Resource.Id.btn_addTransact);

            btnCreateTransact.Click += (s, e) =>
            {
                var sum = root.FindViewById<EditText>(Resource.Id.createTrSum).Text;

                if (!Service.Transactions.CreateTransaction(_user.Id, _selectedCorrId, sum))
                    ErrorClass.AddError(Context, "���������� ��������, ����� ��������� ��� ������");

                var indexPage = new IndexPage();
                FragmentManager.BeginTransaction().Replace(Resource.Id.main, indexPage).Commit();
            };

            exit.Click += (s, e) =>
            {
                UserContext.Logout();
                var main = new Intent(Context, typeof(MainActivity));
                StartActivity(main);
            };

            return root;
        }

        private void OnSpinnerItemSelect(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            var t = _recipients.Result[e.Position];
            _selectedCorrId = t.Id;
        }
    }
}