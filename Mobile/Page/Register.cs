using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using Fragment = Android.Support.V4.App.Fragment;
using Mobile.Model;
using Mobile.Contexts;
using Mobile.Helper;

namespace Mobile.Page
{
    public class Register: BaseFragment
    {
        public static Fragment NewInstance(Context context)
        {
            Register register = new Register();
            return register;
        }
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            ViewGroup root = (ViewGroup)inflater.Inflate(Resource.Layout.Register, null);
            var btnReg = root.FindViewById<Button>(Resource.Id.btn_register);
            var firstName = root.FindViewById<EditText>(Resource.Id.reg_firstName);
            var lastName = root.FindViewById<EditText>(Resource.Id.reg_lastName);
            var email = root.FindViewById<EditText>(Resource.Id.reg_email);
            var password = root.FindViewById<EditText>(Resource.Id.reg_password);
            var confirmPassword = root.FindViewById<EditText>(Resource.Id.reg_confirmPassword);

            btnReg.Click += (sender, e) =>
            {
                RegisterModel register = Service.Users.Register(firstName.Text, lastName.Text, email.Text, password.Text, confirmPassword.Text);
                switch (register.Error)
                {
                    case 0:
                        {
                            UserContext.Login(register.Result);
                            var main = new Intent(Context, typeof(MainActivity));
                            StartActivity(main);
                            break;
                        }
                    case 1:
                        { ErrorClass.AddError(Context, "����������� ������"); break; }
                    case 2:
                        { ErrorClass.AddError(Context, "������������ � ����� email ��� ����������"); break; }
                    case 3:
                        { ErrorClass.AddError(Context, "������� �������� ������"); break; }
                    case 4:
                        { ErrorClass.AddError(Context, "������ �� ���������"); break; }
                    case 5:
                        { ErrorClass.AddError(Context, "������������ ������������� ����"); break; }
                    case 6:
                        { ErrorClass.AddError(Context, "������������ email"); break; }
                };
            };

            return root;
        }
    }
}