using System.Linq;

using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using Fragment = Android.Support.V4.App.Fragment;
using Mobile.Contexts;
using Mobile.Model;
using Mobile.Helper;

namespace Mobile.Page
{
    public class IndexPage: BaseFragment
    {
        private User _user;

        public static Fragment NewInstance(Context context)
        {
            IndexPage indexPage = new IndexPage();
            return indexPage;
        }
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            ViewGroup root = (ViewGroup)inflater.Inflate(Resource.Layout.Index, null);

            _user = Service.Users.GetUser(UserContext.GetToken()).Result;

            var userName = root.FindViewById<TextView>(Resource.Id.userName);
            userName.TextSize = 16;
            userName.Text = "������������, " + _user.FullName + "!";

            var ballance = root.FindViewById<TextView>(Resource.Id.balance);
            ballance.Text = "��� ������:" + _user.Balance;

            var exit = root.FindViewById<TextView>(Resource.Id.exit);
            exit.Text = "�����";

            var transactions = Service.Transactions.GetTransactions(_user.Id);

            if (transactions.Result != null)
            {
                var listView = root.FindViewById<ListView>(Resource.Id.listTransact);
                listView.Adapter = new TransactionsAdapter(Activity, transactions.Result.ToArray(), _user.Id);
            }

            var btnCreateTransact = root.FindViewById<Button>(Resource.Id.btn_createTransact);

            btnCreateTransact.Click += (s, e) =>
            {
                var _createTransaction = new CreateTransaction();
                FragmentManager.BeginTransaction().Replace(Resource.Id.main, _createTransaction).Commit();
            };

            exit.Click += (s, e) =>
            {
                UserContext.Logout();
                var main = new Intent(Context, typeof(MainActivity));
                StartActivity(main);
            };

            return root;
        }
    }
}