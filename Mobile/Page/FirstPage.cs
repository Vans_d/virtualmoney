using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using Fragment = Android.Support.V4.App.Fragment;
using Mobile.Contexts;
using Mobile.Model;
using Mobile.Helper;

namespace Mobile.Page
{
    public class FirstPage: BaseFragment
    {
        public static Fragment NewInstance(Context context)
        {
            FirstPage firstPage = new FirstPage();
            return firstPage;
        }
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            ViewGroup root = (ViewGroup)inflater.Inflate(Resource.Layout.FirstActive, null);
            var email = root.FindViewById<EditText>(Resource.Id.l_email);
            var password = root.FindViewById<EditText>(Resource.Id.l_password);
            var btnLogin = root.FindViewById<Button>(Resource.Id.btn_login);
            var btnRegistration = root.FindViewById<Button>(Resource.Id.btn_register);

            btnLogin.Click += (s, e) =>
            {
                LoginModel login = Service.Users.Login(email.Text, password.Text);

                switch (login.Error)
                {
                    case 0:
                        {
                            UserContext.Login(login.Result);
                            var main = new Intent(Context, typeof(MainActivity));
                            StartActivity(main);
                            break;
                        }
                    case 1:
                        { ErrorClass.AddError(Context, "����������� ������"); break; }
                    case 2:
                        { ErrorClass.AddError(Context, "������������ email �/��� ������"); break; }
                    case 3:
                        { ErrorClass.AddError(Context, "������� ������������� ����"); break; }
                }

                
            };

            btnRegistration.Click += (s, e) =>
            {
                var register = new Register();
                FragmentManager.BeginTransaction().Replace(Resource.Id.mainFirst, register).Commit();
            };

            return root;
        }
    }
}