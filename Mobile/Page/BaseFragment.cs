using Mobile.Contexts;
using Fragment = Android.Support.V4.App.Fragment;

namespace Mobile.Page
{
    public class BaseFragment : Fragment
    {
        private ServiceContext _service;
        protected ServiceContext Service => _service ?? (_service = new ServiceContext());
    }
}