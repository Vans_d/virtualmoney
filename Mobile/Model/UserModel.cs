using System.Collections.Generic;

namespace Mobile.Model
{
    public class UserModel
    {
        public int Error { get; set; }
        public User Result { get; set; }
    }

    public class RecipientsModel
    {
        public int Error { get; set; }
        public IList<User> Result { get; set; }
    }

    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public decimal Balance { get; set; }

        public string FullName => FirstName + " " + LastName;
    }
}