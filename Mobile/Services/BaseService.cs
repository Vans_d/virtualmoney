using System;
using System.Net;
using System.IO;
using System.Json;

namespace Mobile.Services
{
    public abstract class BaseService
    {
        protected static string Domain = "http://192.168.100.2:80/";

        private delegate object Builder(string url);

        protected object GetRequest(string url)
        {
            Builder build = GetResponse;
            IAsyncResult resultObj = build.BeginInvoke(url, null, null);
            return build.EndInvoke(resultObj);
        }

        private object GetResponse(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(url));
            request.ContentType = "application/json";
            request.Method = "GET";

            using (WebResponse response = request.GetResponse())
            {
                using (Stream stream = response.GetResponseStream())
                {
                    JsonValue jsonDoc = JsonValue.Load(stream);
                    return jsonDoc;
                }
            }
        }
    }
}