using Mobile.Model;
using Newtonsoft.Json;

namespace Mobile.Services
{
    public class UsersService: BaseService
    {
        public LoginModel Login(string email, string password)
        {
            var url = $"{Domain}api/Account/Login?Email={email}&Password={password}";
            return JsonConvert.DeserializeObject<LoginModel>(GetRequest(url).ToString());
        }

        public RegisterModel Register(string firstName, string lastName, string email, string password, string confirmPassword)
        {
            var url = $"{Domain}api/Account/Register?FirstName={firstName}&LastName={lastName}&Email={email}&Password={password}&ConfirmPassword={confirmPassword}";
            return JsonConvert.DeserializeObject<RegisterModel>(GetRequest(url).ToString());
        }

        public UserModel GetUser(string token)
        {
            var url = $"{Domain}api/Account/GetUser?Token={token}";
            return JsonConvert.DeserializeObject<UserModel>(GetRequest(url).ToString());
        }

        public RecipientsModel GetAllRecipients(int userId)
        {
            var url = $"{Domain}api/Account/GetAllRecipients?UserId={userId}";
            return JsonConvert.DeserializeObject<RecipientsModel>(GetRequest(url).ToString());
        } 
    }
}