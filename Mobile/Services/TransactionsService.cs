using Newtonsoft.Json;
using Mobile.Model;

namespace Mobile.Services
{
    public class TransactionsService: BaseService
    {
        public ListTransactModel GetTransactions(int userId)
        {
            var url = $"{Domain}api/Transaction/GetTransactions?UserId={userId}";
            return JsonConvert.DeserializeObject<ListTransactModel>(GetRequest(url).ToString());
        }

        public bool CreateTransaction(int senderId, int recipientId, string sum)
        {
            var url = $"{Domain}api/Transaction/CreateTransaction?SenderId={senderId}&RecipientId={recipientId}&Sum={sum}";
            return JsonConvert.DeserializeObject<bool>(GetRequest(url).ToString());
        }
    }
}