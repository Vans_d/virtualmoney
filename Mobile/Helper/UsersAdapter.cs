using Android.App;
using Android.Views;
using Android.Widget;
using Mobile.Model;

namespace Mobile.Helper
{
    public class UsersAdapter: BaseAdapter<User>
    {
        private readonly User[] _items;
        private readonly Activity _context;
        public UsersAdapter(Activity context, User[] items) : base()
        {
            _context = context;
            _items = items;
        }
        public override long GetItemId(int position)
        {
            return position;
        }
        public override User this[int position] => _items[position];

        public override int Count => _items.Length;

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = _items[position];
            View view = convertView ?? _context.LayoutInflater.Inflate(Resource.Layout.Recipients, null);

            var corr = view.FindViewById<TextView>(Resource.Id.recipient);
            corr.Text = item.FullName;
            corr.TextSize = 16;

            return view;
        }
    }
}