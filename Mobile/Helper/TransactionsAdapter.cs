using Android.App;
using Android.Views;
using Android.Widget;
using Mobile.Model;

namespace Mobile.Helper
{
    public class TransactionsAdapter: BaseAdapter<TransactionModel>
    {
        private readonly TransactionModel[] _items;
        private readonly Activity _context;
        private readonly int _userId;
        public TransactionsAdapter(Activity context, TransactionModel[] items, int userId) : base()
        {
            _context = context;
            _items = items;
            _userId = userId;
        }
        public override long GetItemId(int position)
        {
            return position;
        }
        public override TransactionModel this[int position] => _items[position];

        public override int Count => _items.Length;

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = _items[position];
            View view = convertView ?? _context.LayoutInflater.Inflate(Resource.Layout.Transactions, null);

            view.FindViewById<TextView>(Resource.Id.createdAt).Text = item.CreatedAt.ToString();
            view.FindViewById<TextView>(Resource.Id.sender).Text = item.SenderFullName;
            view.FindViewById<TextView>(Resource.Id.recipient).Text = item.RecipientFullName;

            if (item.UserId == _userId)
            {
                view.FindViewById<TextView>(Resource.Id.sum).Text = "-" + item.Sum.ToString();
                view.FindViewById<TextView>(Resource.Id.balanceAfter).Text = item.SenderBalanceAfter.ToString();
            }
            else
            {
                view.FindViewById<TextView>(Resource.Id.sum).Text = "+" + item.Sum.ToString();
                view.FindViewById<TextView>(Resource.Id.balanceAfter).Text = item.RecipientBalanceAfter.ToString();
            }

            return view;
        }
    }
}