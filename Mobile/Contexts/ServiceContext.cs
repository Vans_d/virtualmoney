using Mobile.Services;

namespace Mobile.Contexts
{
    public class ServiceContext
    {
        private UsersService _usersService;
        public UsersService Users => _usersService ?? (_usersService = new UsersService());

        private TransactionsService _transactionsService;
        public TransactionsService Transactions => _transactionsService ?? (_transactionsService = new TransactionsService());
    }
}