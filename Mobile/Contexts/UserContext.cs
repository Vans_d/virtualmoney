using Android.App;
using Android.Content;

namespace Mobile.Contexts
{
    public class UserContext
    {
        private static ISharedPreferencesEditor _editor;

        private static string _token;

        static UserContext()
        {
            ISharedPreferences pref = Application.Context.GetSharedPreferences("UserContext", FileCreationMode.Private);
            _editor = pref.Edit();
            if (!string.IsNullOrEmpty(pref.GetString("token", null)))
                _token = pref.GetString("token", string.Empty);
        }

        public static void Login(string token)
        {
            _token = token;
            _editor.PutString("token", _token).Commit();
        }

        public static void Logout()
        {
            _token = "";
            _editor.Remove("token").Commit();
        }

        public static bool IsLoggedIn()
        {
            return (!string.IsNullOrEmpty(_token));
        }

        public static string GetToken()
        {
            return _token;
        }
    }
}