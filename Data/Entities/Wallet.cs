﻿using System.ComponentModel.DataAnnotations;

namespace Data.Entities
{
    public class Wallet : BaseEntity
    {
        [Required]
        public decimal Balance { get; set; }

        public int UserId { get; set; }
    }
}
