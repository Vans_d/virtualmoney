﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Data.Entities
{
    public class Transaction : BaseEntity
    {
        [Required]
        public Guid Uid { get; set; }

        [Required]
        public int SenderId { get; set; }

        [Required]
        public int RecipientId { get; set; }

        [Required]
        public decimal Sum { get; set; }

        [Required]
        public decimal SenderBalanceBefore { get; set; }

        [Required]
        public decimal SenderBalanceAfter { get; set; }

        [Required]
        public decimal RecipientBalanceBefore { get; set; }

        [Required]
        public decimal RecipientBalanceAfter { get; set; }
    }
}
