﻿using System;

namespace Data
{
    public interface IUnitOfWork : IDisposable
    {
        void Commit();
    }
    public sealed class UnitOfWork : IUnitOfWork
    {
        private IDataContext _dataContext;

        public UnitOfWork(IDataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void Commit()
        {
            _dataContext.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
                if (_dataContext != null)
                {
                    _dataContext.Dispose();
                    _dataContext = null;
                }
        }
    }
}
