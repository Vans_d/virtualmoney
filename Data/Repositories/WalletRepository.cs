﻿using Data.Entities;
using Data.Repositories.Interfaces;

namespace Data.Repositories
{
    public class WalletRepository : BaseRepository<Wallet>, IWalletRepository
    {
        public WalletRepository(IDataContext dataContext) : base(dataContext)
        {
        }

    }
}
