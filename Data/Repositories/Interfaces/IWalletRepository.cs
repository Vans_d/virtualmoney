﻿using System.Threading.Tasks;
using Data.Entities;

namespace Data.Repositories.Interfaces
{
    public interface IWalletRepository : IBaseRepository<Wallet>
    {
    }
}
