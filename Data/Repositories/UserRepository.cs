﻿using Data.Entities;
using Data.Repositories.Interfaces;

namespace Data.Repositories
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(IDataContext dataContext) : base(dataContext)
        {
        }
    }
}
