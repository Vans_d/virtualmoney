﻿using Data.Entities;
using Data.Repositories.Interfaces;

namespace Data.Repositories
{
    public class TransactionRepository : BaseRepository<Transaction>, ITransactionRepository
    {
        public TransactionRepository(IDataContext dataContext) : base(dataContext)
        {
        }
    }
}
