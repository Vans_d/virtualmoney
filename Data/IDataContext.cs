﻿using System;
using Data.Entities;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Data
{
    public interface IDataContext : IDisposable
    {
        DbSet<User> Users { get; set; }
        DbSet<Wallet> Wallets { get; set; }
        DbSet<Transaction> Transactions { get; set; }

        IDbSet<TEntity> Set<TEntity>() where TEntity : class;
        EntityState GetEntityState<TEntity>(TEntity entity) where TEntity : class;
        TEntity AttachEntity<TEntity>(TEntity entity, EntityState? withState = null) where TEntity : class;

        int SaveChanges();
    }
}
