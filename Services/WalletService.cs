﻿using System;
using Data;
using Data.Entities;
using Data.Repositories.Interfaces;
using Services.Interfaces;

namespace Services
{
    public class WalletService : BaseService<Wallet>, IWalletService
    {
        public WalletService(IUnitOfWork unitOfWork, IBaseRepository<Wallet> baseRepository) : base(unitOfWork, baseRepository)
        {
        }

        public Wallet GetWalletByUserId(int userId)
        {
            return Repository.GetBy(w => w.UserId == userId);
        }

        public Wallet CreateWallet(int userId)
        {
            var wallet = new Wallet
            {
                Balance = 500,
                CreatedAt = DateTime.UtcNow,
                UserId = userId
            };

            Repository.Add(wallet);
            UnitOfWork.Commit();

            return wallet;
        }
    }
}
