﻿using System;
using System.Linq;
using Data.Entities;
using Services.Interfaces;
using Common;
using Data.Repositories.Interfaces;
using Data;

namespace Services
{
    public class UserService : BaseService<User>, IUserService
    {
        public UserService(IUnitOfWork unitOfWork, IBaseRepository<User> baseRepository) : base(unitOfWork, baseRepository)
        {
        }

        public User CreateUser(string firstName, string lastName, string email, string password)
        {
            var user = new User
            {
                FirstName = firstName,
                LastName = lastName,
                Email = email,
                PasswordHash = Cryptographer.ComputeSha512Hash(password),
                Token = Guid.NewGuid(),
                CreatedAt = DateTime.UtcNow,
            };
        
            Repository.Add(user);
            UnitOfWork.Commit();

            ServiceLocator.Get<IWalletService>().CreateWallet(user.Id);

            return user;
        }

        public User GetUserByEmail(string email)
        {
            return Repository.GetBy(u => u.Email == email);
        }

        public User GetUserById(int userId)
        {
            return Repository.GetById(userId);
        }

        public User GetUserByToken(Guid token)
        {
            return Repository.GetBy(u => u.Token == token);
        }

        public IQueryable<User> GetAllRecipients(int userId)
        {
            return Repository.GetMany(u => u.Id != userId);
        }

        public bool ValidateUser(string email, string password, out User user)
        {
            user = GetUserByEmail(email);
            
            return user != null && user.PasswordHash == Cryptographer.ComputeSha512Hash(password);
        }

        public bool ValidateUser(string email, string password)
        {
            var user = GetUserByEmail(email);

            return user != null && user.PasswordHash == Cryptographer.ComputeSha512Hash(password);
        }

        public void AddSum(int userId, int recipientId, decimal sum)
        {
            ServiceLocator.Get<IWalletService>().GetWalletByUserId(userId).Balance -= sum;
            ServiceLocator.Get<IWalletService>().GetWalletByUserId(recipientId).Balance += sum;

            UnitOfWork.Commit();
        }

        public bool IsExistEmail(string email)
        {
            return GetUserByEmail(email) != null;
        }
    }
}
