﻿using Data.Entities;

namespace Services.Interfaces
{
    public interface IWalletService : IBaseService<Wallet>
    {
        Wallet GetWalletByUserId(int userId);
        Wallet CreateWallet(int userId);
    }
}
