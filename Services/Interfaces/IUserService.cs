﻿using System;
using System.Linq;
using Data.Entities;

namespace Services.Interfaces
{
    public interface IUserService : IBaseService<User>
    {
        User CreateUser(string firstName, string lastName, string email, string password);
        User GetUserByEmail(string email);
        User GetUserById(int userId);
        User GetUserByToken(Guid token);
        IQueryable<User> GetAllRecipients(int userId);
        void AddSum(int userId, int recipientId, decimal sum);
        bool ValidateUser(string email, string password, out User user);
        bool ValidateUser(string email, string password);
        bool IsExistEmail(string email);
    }
}
