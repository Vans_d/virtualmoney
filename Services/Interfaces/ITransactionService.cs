﻿using System.Linq;
using Data.Entities;

namespace Services.Interfaces
{
    public interface ITransactionService : IBaseService<Transaction>
    {
        IQueryable<Transaction> GetTransactionsByUserId(int userId);

        Transaction GetTransaction(int transactionId);

        Transaction CreateTransaction(int senderId, int recipientId, decimal sum);
    }
}
