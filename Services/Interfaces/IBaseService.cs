﻿using System.Collections.Generic;
using Data.Entities;

namespace Services.Interfaces
{
    public interface IBaseService<TEntity> where TEntity : BaseEntity
    {
        void Create(TEntity entity);
        void Delete(TEntity entity);
        IEnumerable<TEntity> GetAll();
        void Edit(TEntity entity);
    }
}
