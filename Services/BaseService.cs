﻿using System;
using System.Collections.Generic;
using Data;
using Data.Entities;
using Data.Repositories.Interfaces;
using Services.Interfaces;

namespace Services
{
    public abstract class BaseService<TEntity> : IBaseService<TEntity> where TEntity : BaseEntity
    {
        protected IUnitOfWork UnitOfWork;
        protected IBaseRepository<TEntity> Repository;

        protected BaseService(IUnitOfWork unitOfWork, IBaseRepository<TEntity> baseRepository)
        {
            UnitOfWork = unitOfWork;
            Repository = baseRepository;
        }

        public void Create(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            Repository.Add(entity);
            UnitOfWork.Commit();
        }

        public void Delete(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            Repository.Delete(entity);
            UnitOfWork.Commit();
        }

        public IEnumerable<TEntity> GetAll()
        {
            return Repository.GetAll();
        }

        public void Edit(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            Repository.Edit(entity);
            UnitOfWork.Commit();
        }
    }
}
