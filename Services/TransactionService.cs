﻿using System;
using System.Linq;
using Common;
using Data;
using Data.Entities;
using Data.Repositories.Interfaces;
using Services.Interfaces;

namespace Services
{
    public class TransactionService : BaseService<Transaction>, ITransactionService
    {
        public TransactionService(IUnitOfWork unitOfWork, IBaseRepository<Transaction> baseRepository) : base(unitOfWork, baseRepository)
        {
        }

        public IQueryable<Transaction> GetTransactionsByUserId(int userId)
        {
            return Repository.GetMany(t => t.SenderId == userId || t.RecipientId == userId);
        }

        public Transaction GetTransaction(int transactionId)
        {
            return Repository.GetById(transactionId);
        }

        public Transaction CreateTransaction(int senderId, int recipientId, decimal sum)
        {
            var userBalance = ServiceLocator.Get<IWalletService>().GetWalletByUserId(senderId).Balance;
            var recipientBalance = ServiceLocator.Get<IWalletService>().GetWalletByUserId(recipientId).Balance;
            var transaction = new Transaction
            {
                SenderId = senderId,
                RecipientId = recipientId,
                Uid = Guid.NewGuid(),
                CreatedAt = DateTime.UtcNow,
                Sum = sum,
                SenderBalanceBefore = userBalance,
                SenderBalanceAfter = userBalance - sum,
                RecipientBalanceBefore = recipientBalance,
                RecipientBalanceAfter = recipientBalance + sum
            };

            Repository.Add(transaction);
            UnitOfWork.Commit();

            ServiceLocator.Get<IUserService>().AddSum(senderId, recipientId, sum);

            return transaction;
        }
    }
}
