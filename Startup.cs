﻿using System;
using System.Security.Claims;
using System.Web.Helpers;
using System.Web.Http;
using Autofac;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using ParrotWings.App;

namespace ParrotWings
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();

            var builder = new ContainerBuilder();

            AppStart.ConfigureBuilder(builder);
            var container = builder.Build();

//            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);


            ConfigureAuth(app, container);

            WebApiConfig.Register(config);
            app.UseWebApi(config);

            //RouteConfig.RegisterRoutes();
        }

        public void ConfigureAuth(IAppBuilder app, IContainer container)
        {
            app.UseCors(CorsOptions.AllowAll);

            // Use OAuth tokens for API only
            app.UseOAuthBearerTokens(new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/api/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                AllowInsecureHttp = true
            });

            // Use cookie auth for backend part only
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                SlidingExpiration = true,
                ExpireTimeSpan = TimeSpan.FromHours(2)
            });

            AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.NameIdentifier;
        }
    }
}